parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

function mapbox_authed() {
    if [ -z $AWS_SESSION_TOKEN ]; then
        echo ""
    else
        echo "⎔ AUTHED "
    fi
}
export PS1="\[\033[01;34m\]\w \[\033[32m\]\$(parse_git_branch)\[\033[01;33m\] \$(mapbox_authed)\[\033[00m\]> "
